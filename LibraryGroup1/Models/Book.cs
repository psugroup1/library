﻿using Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Book
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public SubjectArea SubjectArea { get; set; }
        public string ISBN { get; set; }
        public string BookDescription { get; set; }
        public bool Lendable { get; set; }

        public Book()
        {

        }
    }
}
