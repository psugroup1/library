﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Enums
{
    public enum SubjectArea
    {
        ComputerScience = 0,
        Economics = 1,
        Psychology = 2,
        Law = 3
    }
}
