﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class OutOfHouseLoan
    {
        public DateTime DateCreated { get; set; }
        public DateTime DateReturned { get; set; }
        public int LibraryId { get; set; }
        public int CopyId { get; set; }
        public int OutOfHouseLoanId { get; set; }
    }
}
