﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Enums;

namespace Models
{
    public class Person
    {
        public string SSN { get; set; }
        public string Name { get; set; }
        public string CampusName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int CardId { get; set; }
        public DateTime CardExpirationDate { get; set; }
        public PersonType PersonType { get; set; }

        public Person()
        {
            
        }
    }
}
