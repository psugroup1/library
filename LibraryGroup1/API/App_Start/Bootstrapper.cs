﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using API.Controllers;
using Repositories;

namespace API.App_Start
{
    public class Bootstrapper
    {
        public static ILifetimeScope InitializeContainer()
        {
            var config = GlobalConfiguration.Configuration;

            var builder = new ContainerBuilder();
            RegisterComponents(builder);
            ILifetimeScope container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            return container;
        }

        private static void RegisterComponents(ContainerBuilder builder)
        {
            builder.RegisterType<BookController>().As<IBookController>();
            builder.RegisterType<PersonController>().As<IPersonController>();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<BookRepository>().As<IBookRepository>();
            builder.RegisterType<PersonRepository>().As<IPersonRepository>();
            builder.RegisterType<LoanRepository>().As<ILoanRepository>();
            builder.Register(c =>
            {
                var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LibraryGroup1ConnectionString"].ConnectionString);
                connection.Open();
                return connection;
            }).As<SqlConnection>().SingleInstance();
        }
    }
}
