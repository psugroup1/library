﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    public interface IBookController
    {
        Task<IHttpActionResult> GetSingleBook(string isbn);
        Task<IHttpActionResult> SearchBookTitle(string searchString);
        Task<IHttpActionResult> SearchBookAuthor(string searchString);
        bool CheckBookLendable(int copyId);
        int CheckBookIsHome(string isbn);
        bool CheckCopyIsHome(int copyId);
        bool CheckCopyExists(int copyId);
    }
}
