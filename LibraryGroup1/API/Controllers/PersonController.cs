﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Models;
using Repositories;

namespace API.Controllers
{
    [RoutePrefix("Person")]
    public class PersonController : ApiController, IPersonController
    {
        private readonly IPersonRepository _personRepository;
        public PersonController(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        [HttpGet, Route("FindPerson")]
        public async Task<IHttpActionResult> FindPerson(int cardId)
        {
            var returnPerson = await  _personRepository.FetchPerson(cardId);
            if (returnPerson == null)
            {
                return NotFound();
            }
            return Ok(returnPerson);
        }

        [HttpGet, Route("GetAllPersons")]
        public async Task<IHttpActionResult> GetAllPersons()
        {

            var returnPersons = await _personRepository.FetchAllPersons();
            if (returnPersons == null)
            {
                return NotFound();
            }
            return Ok(returnPersons);
        }

        public int CheckPersonLoans(int cardId)
        {
            return _personRepository.CheckPersonLoans(cardId);
        }

        public bool ValidateCardId(int cardId)
        {
            var cardExpirationDate = _personRepository.GetCardExpirationDate(cardId);
            if (cardExpirationDate > DateTime.Now.AddDays(7) || cardExpirationDate == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}