﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Models;
using Repositories;

namespace API.Controllers
{
    [RoutePrefix("Loan")]
    public class LoanController : ApiController, ILoanController
    {
        private readonly IBookController _bookController;
        private readonly IPersonController _personController;
        private readonly ILoanRepository _loanRepository;
        public LoanController(IBookController bookController, IPersonController personController, ILoanRepository loanRepository)
        {
            _bookController = bookController;
            _personController = personController;
            _loanRepository = loanRepository;
        }

        [HttpGet, Route("CreateLoan")]
        public async Task<IHttpActionResult> CreateLoan(int cardId, int copyId)
        {
            IHttpActionResult returnResult = null;
            if (!_personController.ValidateCardId(cardId))
            {
                returnResult = Content(HttpStatusCode.BadRequest, "The users card has expired");
            }
            else
            {
                int amountOfLoans = _personController.CheckPersonLoans(cardId);
                if (amountOfLoans >= 5)
                {
                    returnResult = BadRequest("The user has maximum amount of loans");
                }
                else if (!_bookController.CheckCopyExists(copyId))
                {
                    returnResult = BadRequest("This copy doesn't exist");
                }
                else if (!_bookController.CheckBookLendable(copyId))
                {
                    //Bog kan ikke udlånes fordi typen ikke er "bog"
                    returnResult = BadRequest("This book cant be lent");
                }

                else if (!_bookController.CheckCopyIsHome(copyId))
                {
                    //Bog kan ikke udlånes fordi den allerede er udlånt
                    returnResult = BadRequest("This book is not home");
                }
                else
                {
                    //DEJLIGT! Bog kan udlånes
                    Loan createdLoan;
                    //try
                    //{
                        createdLoan = await _loanRepository.CreateLoan(cardId, copyId);
                    //}
                    //catch (Exception e) //Vi catcher alt, således at vi ikke returnerer hele stacktracen til bruger/hacker
                    //{
                    //    createdLoan = null;
                    //}
                    if (createdLoan == null)
                    {
                        //TODO Log error in errorDB
                        returnResult = BadRequest("Unknown exception at serverside");
                    }
                    else
                    {
                    returnResult = Ok(createdLoan);
                    }
                }
            }
            return returnResult;
        }

        [HttpGet, Route("ReturnLoan")]
        public async Task<IHttpActionResult> ReturnLoan(int copyId)
        {
            IHttpActionResult returnResult = null;
            var returnedLoan = await _loanRepository.ReturnLoan(copyId);
            returnResult = Ok(returnedLoan);
            return returnResult;
        }

        [HttpGet, Route("OutOfHouseLoan")]
        public async Task<IHttpActionResult> CreateOutOfHouseLoan(int copyId, int libId)
        {
            IHttpActionResult returnResult = null;
            if (!_bookController.CheckBookLendable(copyId))
            {
                //Bog kan ikke udlånes fordi typen ikke er "bog"
                returnResult = Content(HttpStatusCode.BadRequest, "This book cant be lent");
            }

            else if (!_bookController.CheckCopyIsHome(copyId))
            {
                //Bog kan ikke udlånes fordi den allerede er udlånt
                returnResult = Content(HttpStatusCode.BadRequest, "This book is not home!");
            }
            else
            {
                var returnedOutOfHouseLoan = await _loanRepository.CreateOutOfHouseLoan(copyId, libId);
                returnResult = Ok(returnedOutOfHouseLoan);
            }
            return returnResult;
        }

        [HttpGet, Route("ReturnOutOfHouseLoan")]
        public async Task<IHttpActionResult> ReturnOutOfHouseLoan(int copyId)
        {
            IHttpActionResult returnResult = null;
            var returnedLoan = await _loanRepository.ReturnOutOfHouseLoan(copyId);
            returnResult = Ok(returnedLoan);
            return returnResult;
        }
    }
}