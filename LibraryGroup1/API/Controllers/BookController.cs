﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Repositories;

namespace API.Controllers
{
    [RoutePrefix("Book")]
    public class BookController : ApiController, IBookController
    {
        private readonly IBookRepository _bookRepository;
        public BookController(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        /// <summary>
        /// Search for book by isbn
        /// </summary>
        /// <remarks>
        /// Gets the book that has the wanted isbn
        /// </remarks>
        /// <param name="isbn"></param>
        /// <returns></returns>
        [ResponseType(typeof(Book))]
        [HttpGet, Route("GetSingleBook")]
        public async Task<IHttpActionResult> GetSingleBook(string isbn)
        {
            var returnBook = await _bookRepository.FetchBook(isbn);
            if (returnBook == null)
            {
                return NotFound();
            }
            return Ok(returnBook);
        }

        /// <summary>
        /// Search for book by title
        /// </summary>
        /// <remarks>
        /// Gets a list of books that matches the searchString by title
        /// </remarks>
        /// <param name="searchString"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<Book>))]
        [HttpGet, Route("SearchBookTitle")]
        public async Task<IHttpActionResult> SearchBookTitle(string searchString)
        {
            var returnList = await _bookRepository.SearchBookTitle(searchString);
            if (returnList == null)
            {
                return NotFound();
            }
            return Ok(returnList);
        }

        /// <summary>
        /// Search for book by author
        /// </summary>
        /// <remarks>
        /// Gets a list of books that matches the searchString by author
        /// </remarks>
        /// <param name="searchString"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<Book>))]
        [HttpGet, Route("SearchBookAuthor")]
        public async Task<IHttpActionResult> SearchBookAuthor(string searchString)
        {
            var returnList = await _bookRepository.SearchBookAuthor(searchString);
            if (returnList == null)
            {
                return NotFound();
            }
            return Ok(returnList);
        }

        /// <summary>
        /// Get all books
        /// </summary>
        /// <remarks>
        /// Gets all the books listed in the database
        /// </remarks>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<Book>))]
        [HttpGet, Route("GetAllBooks")]
        public async Task<IHttpActionResult> GetAllBooks()
        {
            var returnBook = await _bookRepository.FetchAllBooks();
            if (returnBook == null)
            {
                return NotFound();
            }
            return Ok(returnBook);
        }
        
        /// <summary>
        /// Check book availability
        /// </summary>
        /// <remarks>
        /// Get amount of books currently available
        /// </remarks>
        /// <param name="isbn"></param>
        /// <returns></returns>
        [ResponseType(typeof(int))]
        [HttpGet, Route("CheckBookAvailability")]
        public int CheckBookIsHome(string isbn)
        {
            return _bookRepository.CheckBookIsHome(isbn);
        }

        public bool CheckBookLendable(int copyId)
        {
            return _bookRepository.CheckBookLendable(copyId);
        }

        public bool CheckCopyIsHome(int copyId)
        {
            return _bookRepository.CheckCopyIsHome(copyId);
        }

        public bool CheckCopyExists(int copyId)
        {
            return _bookRepository.CheckCopyExists(copyId);
        }
    }
}