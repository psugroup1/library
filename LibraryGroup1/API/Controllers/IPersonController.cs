﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Models;

namespace API.Controllers
{
    public interface IPersonController
    {
        Task<IHttpActionResult> FindPerson(int cardId);
        Task<IHttpActionResult> GetAllPersons();
        int CheckPersonLoans(int cardId);
        bool ValidateCardId(int cardId);
    }
}
