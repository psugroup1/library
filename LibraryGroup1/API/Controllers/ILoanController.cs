﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Models;

namespace API.Controllers
{
    public interface ILoanController
    {
        Task<IHttpActionResult> CreateLoan(int cardId, int copyId);
        Task<IHttpActionResult> ReturnLoan(int copyId);
        Task<IHttpActionResult> CreateOutOfHouseLoan(int copyId, int libId);
        Task<IHttpActionResult> ReturnOutOfHouseLoan(int copyId);

    }
}
