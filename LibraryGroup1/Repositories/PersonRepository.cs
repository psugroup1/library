﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Models;

namespace Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly SqlConnection _connection;

        public PersonRepository(SqlConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            _connection = connection;
        }

        public async Task<Person> FetchPerson(int cardId)
        {
            Person returnPerson = null;
            var sqlPerson = "SELECT * " +
                            "FROM PERSONS " +
                            "INNER JOIN CAMPUS " +
                            "ON PERSONS.CampusId = CAMPUS.CampusId " +
                            "WHERE CardId = @cardId";
            returnPerson = (await _connection.QueryAsync<Person>(sqlPerson,
            new
            {
                cardId = cardId
            })).
            SingleOrDefault();
            return returnPerson;
        }

        public int CheckPersonLoans(int cardId)
        {
            int returnInt = 0;
            var sqlAmountOfLoans = "SELECT count(*)" +
                                   "FROM dbo.Loans " +
                                   "WHERE PersonCardId = @cardId AND DateReturned IS NULL;";
            returnInt = (_connection.Query<int>(sqlAmountOfLoans,
            new
            {
                cardId = cardId
            })).SingleOrDefault();
            return returnInt;
        }

        public async Task<List<Person>> FetchAllPersons()
        {
            List<Person> returnPersonList = null;
            var sqlPerson = "SELECT * " +
                            "FROM PERSONS " +
                            "INNER JOIN CAMPUS " +
                            "ON PERSONS.CampusId = CAMPUS.CampusId ";
            returnPersonList = (await _connection.QueryAsync<Person>(sqlPerson)).ToList();
            return returnPersonList;
        }

        public DateTime? GetCardExpirationDate(int cardId)
        {
            var sqlValidatePerson = "Select CardExpirationDate " +
                                    "FROM PERSONS " +
                                    "WHERE CardId = @cardId;";

            DateTime? returnDateTime = (_connection.Query<DateTime?>(sqlValidatePerson, new
            {
                cardId = cardId
            })).SingleOrDefault();
            return returnDateTime;
        }
    }
}
