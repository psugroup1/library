﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Repositories
{
    public interface ILoanRepository
    {
        Task<Loan> CreateLoan(int cardId, int copyId);
        Task<Loan> ReturnLoan(int copyId);
        Task<OutOfHouseLoan> CreateOutOfHouseLoan(int copyId, int libId);
        Task<Loan> ReturnOutOfHouseLoan(int copyId);

    }
}
