﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Models;

namespace Repositories
{
    public class LoanRepository : ILoanRepository
    {
        private readonly SqlConnection _connection;

        public LoanRepository(SqlConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            _connection = connection;
        }
        public async Task<Loan> CreateLoan(int cardId, int copyId)
        {
            Loan loanCreated = null;
            var sqlCreateLoan = "INSERT INTO Loans(PersonCardId, CopyId) " +
                                "VALUES " +
                                "(@cardId, @copyId) " +
                                "SELECT * " +
                                "FROM LOANS " +
                                "WHERE LOANS.LoanId = @@identity; " +
                                "UPDATE Copies " +
                                "Set IsOnLoan = 1 " +
                                "WHERE CopyId = @copyId";

            loanCreated = (await _connection.QueryAsync<Loan>(sqlCreateLoan, new
            {
                cardId = cardId,
                copyId = copyId
            })).SingleOrDefault();
            return loanCreated;

        }

        public async Task<Loan> ReturnLoan(int copyId)
        {
            Loan returnLoan = null;
            var sqlReturnLoan = "UPDATE Loans " +
                                "SET DateReturned = GETDATE() " +
                                "OUTPUT inserted.* " +
                                "WHERE CopyId = @copyId " +
                                "AND DateReturned IS NULL; " +
                                "UPDATE Copies " +
                                "Set IsOnLoan = 0 " +
                                "WHERE CopyId = @copyId; ";

            returnLoan = (await _connection.QueryAsync<Loan>(sqlReturnLoan, new
            {
                copyId = copyId
            })).SingleOrDefault();
            return returnLoan;
        }

        public async Task<OutOfHouseLoan> CreateOutOfHouseLoan(int copyId, int libId)
        {
            OutOfHouseLoan outOfHouseLoanCreated = null;
            var sqlCreateOutOfHouseLoan = "INSERT INTO OutOfHouseLoans(CopyId, LibraryId) " +
                                "VALUES " +
                                "(@copyId, @libId); " +
                                "UPDATE Copies" +
                                "Set IsOnLoan = 1 " +
                                "WHERE CopyId = @copyId; " +
                                "SELECT * " +
                                "FROM OutOfHouseLoans " +
                                "WHERE OutOfHouseLoans.OutOfHouseLoanId = @@identity;";

            outOfHouseLoanCreated = (await _connection.QueryAsync<OutOfHouseLoan>(sqlCreateOutOfHouseLoan, new
            {
                copyId = copyId,
                libId = libId
            })).SingleOrDefault();
            return outOfHouseLoanCreated;
        }
        public async Task<Loan> ReturnOutOfHouseLoan(int copyId)
        {
            Loan returnLoan = null;
            var sqlReturnLoan = "UPDATE OutOfHouseLoans " +
                                "SET DateReturned = GETDATE() " +
                                "OUTPUT inserted.* " +
                                "WHERE CopyId = @copyId " +
                                "AND DateReturned IS NULL; " +
                                "UPDATE Copies " +
                                "Set IsOnLoan = 0 " +
                                "WHERE CopyId = @copyId; ";

            returnLoan = (await _connection.QueryAsync<Loan>(sqlReturnLoan, new
            {
                copyId = copyId
            })).SingleOrDefault();
            return returnLoan;
        }
    }
}
