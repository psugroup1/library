﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Repositories
{
    public interface IPersonRepository
    {
        Task<Person> FetchPerson(int cardId);
        int CheckPersonLoans(int cardId);
        Task<List<Person>> FetchAllPersons();
        DateTime? GetCardExpirationDate(int cardId);
    }
}
