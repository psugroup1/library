﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models;

namespace Repositories
{
    public interface IBookRepository
    {
        Task<Book> FetchBook(string isbn);
        Task<List<Book>> SearchBookTitle(string searchString);
        Task<List<Book>> SearchBookAuthor(string searchString);
        bool CheckBookLendable(int copyId);
        bool CheckCopyIsHome(int copyId);
        int CheckBookIsHome(string isbn);
        Task<List<Book>> FetchAllBooks();
        bool CheckCopyExists(int copyId);
    }
}