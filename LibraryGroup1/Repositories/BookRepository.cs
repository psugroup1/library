﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Models;

namespace Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly SqlConnection _connection;

        public BookRepository(SqlConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            _connection = connection;
        }
        public async Task<Book> FetchBook (string isbn)
        {
            Book returnBook = null;
            var sqlBook = "SELECT * " +
                          "FROM BOOKS " +
                          "WHERE isbn = @isbn";
            returnBook = (await _connection.QueryAsync<Book>(sqlBook, new
            {
                isbn = isbn
            })).SingleOrDefault();
            return returnBook;
        }

        public async Task<List<Book>> SearchBookTitle(string searchString)
        {
            List<Book> returnList = null;
            var sqlSearchBook = "SELECT * " +
                                "FROM BOOKS " +
                                "WHERE Title LIKE '%' + @searchString + '%';";
            returnList = (await _connection.QueryAsync<Book>(sqlSearchBook, new
            {
                searchString = searchString
            })).ToList();
            return returnList;
        }

        public async Task<List<Book>> SearchBookAuthor(string searchString)
        {
            List<Book> returnList = null;
            var sqlSearchBook = "SELECT * " +
                                "FROM BOOKS " +
                                "WHERE Author LIKE '%' + @searchString + '%';";
            returnList = (await _connection.QueryAsync<Book>(sqlSearchBook, new
            {
                searchString = searchString
            })).ToList();
            return returnList;
        }

        public bool CheckBookLendable(int copyId)
        {
            var sqlCheckLendable = "SELECT Lendable " +
                                   "FROM Books as b " +
                                   "INNER JOIN Copies as c ON b.ISBN=c.CopyISBN " +
                                   "WHERE c.CopyId = @copyId;";
            var lendable =_connection.Query<bool>(sqlCheckLendable, new
            {
                copyId = copyId
            }).SingleOrDefault();
            return lendable;
        }

        public bool CheckCopyIsHome(int copyId)
        {
            bool returnBool = false;
            var sqlCheckIsHome = "SELECT IsOnLoan " +
                                 "FROM Copies " +
                                 "WHERE CopyId = @copyId ";
            returnBool = _connection.Query<Boolean>(sqlCheckIsHome, new
            {
                copyId = copyId
            }).SingleOrDefault();
            return !returnBool;
        }

        public int CheckBookIsHome(string isbn)
        {
            int returnInt = 0;
            var sqlCheckBookIsHome = "Select Count(*) " +
                                     "From Books as b " +
                                     "INNER JOIN Copies as c ON b.ISBN = c.CopyISBN " +
                                     "WHERE b.ISBN = @ISBN AND c.IsOnLoan = 0;";
            returnInt = _connection.Query<int>(sqlCheckBookIsHome, new
            {
                ISBN = isbn
            }).SingleOrDefault();
            return returnInt;
        }

        public async Task<List<Book>> FetchAllBooks()
        {
            List<Book> returnList = null;
            var sqlSearchBook = "SELECT * " +
                                "FROM BOOKS";
            returnList = (await _connection.QueryAsync<Book>(sqlSearchBook)).ToList();
            return returnList;
        }

        public bool CheckCopyExists(int copyId)
        {
            var sqlCheckCopy = "SELECT Count(*) " +
                               "FROM COPIES " +
                               "WHERE CopyId = @copyId";
            int returnInt = (_connection.Query<int>(sqlCheckCopy, new
            {
                copyId = copyId
            })).SingleOrDefault();
            if (returnInt == 0) return false;
            return true;
        }
    }
}