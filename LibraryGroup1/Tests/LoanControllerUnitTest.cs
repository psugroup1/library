﻿using System;
using System.Threading.Tasks;
using System.Web.Http.Results;
using API.Controllers;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using Moq;
using Repositories;

namespace Tests
{
    [TestClass]
    public class LoanControllerUnitTest
    {
        private Mock<ILoanRepository> _mockLoanRep = null;
        private LoanController _loanController = null;

        private Mock<IPersonController> _mockPersonController = null;
        private Mock<IBookController> _mockBookController = null;
        private Loan _mockLoan;
        [TestInitialize]
        public void Setup()
        {
            _mockLoanRep = new Mock<ILoanRepository>();
            _mockBookController = new Mock<IBookController>();
            _mockPersonController = new Mock<IPersonController>();
            _loanController = new LoanController(_mockBookController.Object, _mockPersonController.Object, _mockLoanRep.Object); //inject mock object som stub
            _mockLoan = new Loan
            {
                PersonCardID = 1,
                DateReturned = DateTime.MinValue,
                CopyId = 1,
                DateCreated = DateTime.Now,
                LoanId = 1
            };
        }
        [TestMethod]
        public void TP001TestCreateLoanPositiveCopyId()
        {
            int testCardId = 1;
            int testCopyId = 1;
            _mockPersonController.Setup(x => x.CheckPersonLoans(testCardId)).Returns(0);
            _mockPersonController.Setup(x => x.ValidateCardId(testCardId)).Returns(true);
            _mockBookController.Setup(x => x.CheckBookLendable(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyExists(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyIsHome(testCopyId)).Returns(true);
            _mockLoanRep.Setup(x => x.CreateLoan(testCardId, testCopyId)).Returns(Task.FromResult(_mockLoan));
            var createdLoan = _loanController.CreateLoan(testCardId, testCopyId).Result as OkNegotiatedContentResult<Loan>;
            createdLoan.Content.CopyId.Should().Be(testCopyId);
        }

        [TestMethod]
        public void TP002TestCreateLoanNegativeInvalidCopy()
        {
            int testCardId = 1;
            int testCopyId = 1;
            _mockPersonController.Setup(x => x.CheckPersonLoans(testCardId)).Returns(0);
            _mockPersonController.Setup(x => x.ValidateCardId(testCardId)).Returns(true);
            _mockBookController.Setup(x => x.CheckBookLendable(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyIsHome(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyExists(testCopyId)).Returns(false);
            _mockLoanRep.Setup(x => x.CreateLoan(testCardId, testCopyId)).Returns(Task.FromResult(_mockLoan));
            var response = _loanController.CreateLoan(testCardId, testCopyId).Result as BadRequestErrorMessageResult;
            var errorcode = response.Message;
            errorcode.Should().Be("This copy doesn't exist");
        }

        [TestMethod]
        public void TP003TestCreateLoanNegativeCopyIsNotHome()
        {
            int testCardId = 1;
            int testCopyId = 1;
            _mockPersonController.Setup(x => x.CheckPersonLoans(testCardId)).Returns(0);
            _mockPersonController.Setup(x => x.ValidateCardId(testCardId)).Returns(true);
            _mockBookController.Setup(x => x.CheckBookLendable(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyIsHome(testCopyId)).Returns(false);
            _mockBookController.Setup(x => x.CheckCopyExists(testCopyId)).Returns(true);
            _mockLoanRep.Setup(x => x.CreateLoan(testCardId, testCopyId)).Returns(Task.FromResult(_mockLoan));
            var response = _loanController.CreateLoan(testCardId, testCopyId).Result as BadRequestErrorMessageResult;
            var errorcode = response.Message;
            errorcode.Should().Be("This book is not home");
        }

        [TestMethod]
        public void TP004TestCreateLoanNegativeBookNotLendable()
        {
            int testCardId = 1;
            int testCopyId = 1;
            _mockPersonController.Setup(x => x.CheckPersonLoans(testCardId)).Returns(0);
            _mockPersonController.Setup(x => x.ValidateCardId(testCardId)).Returns(true);
            _mockBookController.Setup(x => x.CheckBookLendable(testCopyId)).Returns(false);
            _mockBookController.Setup(x => x.CheckCopyIsHome(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyExists(testCopyId)).Returns(true);
            _mockLoanRep.Setup(x => x.CreateLoan(testCardId, testCopyId)).Returns(Task.FromResult(_mockLoan));
            var response = _loanController.CreateLoan(testCardId, testCopyId).Result as BadRequestErrorMessageResult;
            var errorcode = response.Message;
            errorcode.Should().Be("This book cant be lent");
        }

        [TestMethod]
        public void TP005TestCreateLoanPositiveEquivalenceBoundaryCardId0Loans()
        {
            int testCardId = 1;
            int testCopyId = 1;
            _mockPersonController.Setup(x => x.CheckPersonLoans(testCardId)).Returns(0);
            _mockPersonController.Setup(x => x.ValidateCardId(testCardId)).Returns(true);
            _mockBookController.Setup(x => x.CheckBookLendable(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyExists(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyIsHome(testCopyId)).Returns(true);
            _mockLoanRep.Setup(x => x.CreateLoan(testCardId, testCopyId)).Returns(Task.FromResult(_mockLoan));
            var createdLoan = _loanController.CreateLoan(testCardId, testCopyId).Result as OkNegotiatedContentResult<Loan>;
            createdLoan.Content.PersonCardID.Should().Be(testCardId);
        }

        [TestMethod]
        public void TP006TestCreateLoanPositiveBoundaryCardId4Loans()
        {
            int testCardId = 1;
            int testCopyId = 1;
            _mockPersonController.Setup(x => x.CheckPersonLoans(testCardId)).Returns(4);
            _mockPersonController.Setup(x => x.ValidateCardId(testCardId)).Returns(true);
            _mockBookController.Setup(x => x.CheckBookLendable(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyExists(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyIsHome(testCopyId)).Returns(true);
            _mockLoanRep.Setup(x => x.CreateLoan(testCardId, testCopyId)).Returns(Task.FromResult(_mockLoan));
            var createdLoan = _loanController.CreateLoan(testCardId, testCopyId).Result as OkNegotiatedContentResult<Loan>;
            createdLoan.Content.PersonCardID.Should().Be(testCardId);
        }

        [TestMethod]
        public void TP007TestCreateLoanNegativeEquivalenceBoundaryCardId5Loans()
        {
            int testCardId = 1;
            int testCopyId = 1;
            _mockPersonController.Setup(x => x.CheckPersonLoans(testCardId)).Returns(5);
            _mockPersonController.Setup(x => x.ValidateCardId(testCardId)).Returns(true);
            _mockBookController.Setup(x => x.CheckBookLendable(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyExists(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyIsHome(testCopyId)).Returns(true);
            _mockLoanRep.Setup(x => x.CreateLoan(testCardId, testCopyId)).Returns(Task.FromResult(_mockLoan));
            var response = _loanController.CreateLoan(testCardId, testCopyId).Result as BadRequestErrorMessageResult;
            var errorcode = response.Message;
            errorcode.Should().Be("The user has maximum amount of loans");
        }

        [TestMethod]
        public void TP008TestCreateLoanNegativeBoundaryCardId6Loans()
        {
            //Arrange
            int testCardId = 1;
            int testCopyId = 1;
            _mockPersonController.Setup(x => x.CheckPersonLoans(testCardId)).Returns(6);
            _mockPersonController.Setup(x => x.ValidateCardId(testCardId)).Returns(true);
            _mockBookController.Setup(x => x.CheckBookLendable(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyExists(testCopyId)).Returns(true);
            _mockBookController.Setup(x => x.CheckCopyIsHome(testCopyId)).Returns(true);
            _mockLoanRep.Setup(x => x.CreateLoan(testCardId, testCopyId)).Returns(Task.FromResult(_mockLoan));

            //Act
            var response = _loanController.CreateLoan(testCardId, testCopyId).Result as BadRequestErrorMessageResult;

            //Assert
            var errorcode = response.Message;
            errorcode.Should().Be("The user has maximum amount of loans");
        }
    }
}