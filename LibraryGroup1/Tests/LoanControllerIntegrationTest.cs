﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;

namespace Tests
{
    [TestClass]
    public class LoanControllerIntegrationTest
    {
        [TestMethod]
        public void TP009TestCreateLoanE2E()
        {
            int cardId = 1;
            int copyId = 1;
            var client = GetHttpClient();
            var response = client.GetAsync($"Loan/CreateLoan?cardId={cardId}&copyId={copyId}").Result;
            Loan returnLoan = response.Content.ReadAsAsync<Loan>().Result;
            returnLoan.DateCreated.Date.Should().Be(DateTime.Today);
            response = client.GetAsync($"Loan/ReturnLoan?copyId={copyId}").Result; //Vi afleverer lige bogen tilbage igen
        }

        private HttpClient GetHttpClient()
        {
            var client = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true });
            //client.BaseAddress = new Uri("http://localhost:1659/");
            client.BaseAddress = new Uri("http://librarygroup1.azurewebsites.net/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue($"application/json"));
            return client;
        }
    }
}