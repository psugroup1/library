﻿//using System;
//using System.Threading.Tasks;
//using API.Controllers;
//using FluentAssertions;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Models;
//using Moq;
//using Repositories;
//using System.Web.Http.Results;
//using Models.Enums;

//namespace Tests
//{
//    [TestClass]
//    public class PersonControllerUnitTest
//    {
//        private Person _person1 = null;
//        private Person _person2 = null;
//        private Person _person3 = null;
//        private Mock<IPersonRepository> _mockPersonRep = null;
//        private PersonController _personController = null;

//        [TestInitialize]
//        public void Setup()
//        {
//            _person1 = new Person
//            {
//                CampusName = "Testvej",
//                CardId = 999,
//                Email = "test@testmail.com",
//                Name = "Test Testesen",
//                Phone = "12345678",
//                SSN = "8261852296",
//                CardExpirationDate = DateTime.Now.AddYears(3),
//                PersonType = PersonType.Member
//            };
//            _person2 = new Person
//            {
//                CampusName = "Testvej",
//                CardId = 999,
//                Email = "test@testmail.com",
//                Name = "Test Testesen",
//                Phone = "12345678",
//                SSN = "8261852296",
//                CardExpirationDate = DateTime.Now.AddYears(3),
//                PersonType = PersonType.Member
//            };

//            _mockPersonRep = new Mock<IPersonRepository>();
//            _mockPersonRep.Setup(x => x.FetchPerson(999)).Returns(Task.FromResult(_person1));
//            _mockPersonRep.Setup(x => x.FetchPerson(111)).Returns(Task.FromResult(_person3));
//            _personController = new PersonController(_mockPersonRep.Object); //inject mock object som stub
//            _mockPersonRep.Verify(c=>c.CheckPersonLoans(1), Times.Once); //Eksempel på mock
//        }
//        //Unittest for testing only the personcontroller. personreposotory is mocked. equivalence test
//        [TestMethod]
//        public void FindPersonPositive()
//        {
//            var persResult = _personController.FindPerson(999).Result as OkNegotiatedContentResult<Person>;
//            persResult.Content.PersonType.Should().Be(_person1.PersonType);
//            persResult.Content.CardId.Should().Be(999);
//            persResult.Should().BeOfType<OkNegotiatedContentResult<Person>>(); //Slet nogle asserts
//        }

//        [TestMethod]
//        public void FindPersonNegative()
//        {
//            var persResult = _personController.FindPerson(111).Result;
//            persResult.Should().BeOfType<NotFoundResult>();
//        }

//        [TestCleanup]
//        public void CleanUp()
//        {
//            _person1 = null;
//            _person2 = null;
//            _mockPersonRep = null;
//            _personController = null;
//        }
//    }
//}
